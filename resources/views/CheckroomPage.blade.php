<!DOCTYPE html>
<html>
<head>
	<title>Rooms Select page</title>
    
	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
   

</head>
<body>
	<br><br><br>
	<div class="container" >
        <form  action="{{url('checkroomApi')}}" method="post" class="row g-3">
        @csrf
			<div class="col-md-2">
                <label for="idRoom" class="form-label">Select rooms</label><br>
                <select class="btn btn-secondary dropdown-toggle" name="room" >
                    <option value=""> - Select rooms - </option>
                    @foreach($allRoom as $roomDb)
                        <option value="{{$roomDb->roomtypeId}}" >{{ $roomDb->roomTypename }}</option>    
                    @endforeach
                </select>
			</div>
			 <div class="col-md-4">
				<label for="idRoom" class="form-label">Select date</label>
				<input id="datepicker" name="date" width="270" />
			</div> 
			
			<div class="col-12"><br>
				&nbsp;&nbsp;<button type="submit" class="btn btn-primary">Click for check</button>
                <a class="btn btn-danger" href="/room" role="button">Back to home</a>
			</div>

	
        </form>
        
    </div>

    @if(isset($returnDataApi['date']))
        <div class="container" >
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th scope="col">DATE</th>
                    <th scope="col">ROOM LEFT</th>
                    <th scope="col">PRICE</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>{{ $returnDataApi['date'] }}</th>
                    <td>{{ $returnDataApi['room_left'] }}</td>
                    <td>{{ $returnDataApi['price'] }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    @elseif(isset($returnDataApi['status']))
        <div class="container" >
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th scope="col">STATUS</th>
                    <th scope="col">DETAIL</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>{{ $returnDataApi['status'] }}</th>
                    <td>{{ $returnDataApi['message'] }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    @endif
    


</body>


</html>


<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script>

    $('#datepicker').datepicker({
       uiLibrary: 'bootstrap'
    }); 
</script>