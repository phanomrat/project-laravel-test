<!DOCTYPE html>
<html>
<head>
	<title>Rooms Select page</title>

	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
</head>
<body>
	<br><br><br>
	<div class="container" >

		<div class="col-12">
		<a class="btn btn-success" href="{{url('checkroom')}}" role="button">Click for check rooms</a>
		</div><br><br>

		<form  action="{{url('create')}}" method="post" class="row g-3">
			{{csrf_field()}}
			<div class="col-md-6">
				<label for="nameRoom" class="form-label">Room</label>
				<input type="text" name="nameRoom" class="form-control" id="nameRoom">
			</div>
			<div class="col-md-6">
				<label for="idRoom" class="form-label">Room ID</label>
				<input type="number" name="idRoom" class="form-control" id="idRoom">
			</div>
			
			<div class="col-12">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</form>


		<table class="table table-striped table-hover">
		  <thead>
		    <tr>
		      <th scope="col">#</th>
		      <th scope="col">Room name</th>
		      <th scope="col">Room ID</th>

		    </tr>
		  </thead>
		  <tbody>

		  	@foreach ($allRoom as $key => $rooms)
			
		    <tr>
		      <th scope="row">{{ $key+=1 }}</th>
		      <td>{{ $rooms->roomTypename }}</td>
		      <td>{{ $rooms->roomtypeId }}</td>
		    </tr>
			@endforeach

		  </tbody>
		</table>


</body>



<script src="{{asset('js/bootstrap.min.js')}}"></script>
</html>






