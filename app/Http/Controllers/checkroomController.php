<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\checkroomController;
use Illuminate\Support\Facades\DB;
use App\room; 


class checkroomController extends Controller
{
    public function index()
    {
        $allRoom = DB::table('rooms')->get();
        return view('CheckroomPage', ['allRoom' => $allRoom]);
    }

    public function store(Request $request)
    {

        $this->validate($request,['room' => 'required', 'date' => 'required']);
        $newDate = date("Y-m-d", strtotime($request->date));  

        $url = "http://34.87.142.215/aspire-project/public/booking-box/api-test";
        $data_array = array(
            'date' => $newDate,
            'room_type_id' => $request->room
        );

        $data = http_build_query($data_array);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL ,$url);
        curl_setopt($ch, CURLOPT_POST ,true);
        curl_setopt($ch, CURLOPT_POSTFIELDS ,$data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER ,true);

        $resp = curl_exec($ch);
        
        if($e = curl_exec($ch)){
            $value_decoded = json_decode($e);

            $result = (array) $value_decoded;


        }else{
            $decoded = json_decode($resp);
            foreach($decoded as $key => $val){
                echo $key.' : '.$val.'<br>';
            }
        }
        curl_close($ch);

        $allRoom = DB::table('rooms')->get();
        return view('CheckroomPage', ['allRoom' => $allRoom , 'returnDataApi' => $result ]);
    }

    
}
