<?php

use App\Http\Controller\roomController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('create','roomController');

Route::get('/room','roomController@index');

Route::get('/CreateRoom','roomController@index');

Route::resource('checkroom','CheckroomController');

Route::resource('checkroomApi','CheckroomController');



